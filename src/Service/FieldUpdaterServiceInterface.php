<?php

namespace Drupal\integer_to_decimal\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;

/**
 * Provides an interface for Field Updater services.
 *
 */
interface FieldUpdaterServiceInterface {
  /**
   * Updates a given field with the new precision and scale.
   *
   * @param string $field
   * Machine name of the field
   *
   * @param string $type
   * Field type such as integer, decimal
   *
   * @param $entity_type
   * The entity machine name
   *
   * @param array $bundles
   * The bundles to which the converted field is associated with
   *
   * @param integer $precision
   * precision associated with decimal field type
   *
   * @param integer $scale
   * scale associated with decimal field type
   *
   *
   * @throws InvalidPluginDefinitionException
   */
    public function fieldUpdater(string $field, string $type, $entity_type, array $bundles, int $precision, int $scale);
}
