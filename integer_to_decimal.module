<?php

/**
 * @file
 * The integer_to_decimal module.
 */

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function integer_to_decimal_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the integer_to_decimal module.
    case 'help.page.integer_to_decimal':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Changes field type numeric with data to field type decimal.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function integer_to_decimal_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();

  if ($form_object instanceof EntityForm && $form_id === 'field_config_edit_form') {
    /** @var Drupal\field\Entity\FieldConfig $entity */
    $entity = $form_object->getEntity();
    $fieldType = $entity->getType();

    if ($fieldType === 'integer' && $entity->getTargetEntityTypeId() === 'node') {

      $precisionNumbers = range(10, 32);
      $precision = [];
      foreach ($precisionNumbers as $precisionNumber) {
        $precision[$precisionNumber] = t((string) $precisionNumber);
      }
      $scaleNumbers = range(0, 10);
      $scale = [];
      foreach ($scaleNumbers as $scaleNumber) {
        $scale[$scaleNumber] = t((string) $scaleNumber);
      }

      $form['field_updater_settings'] = [
        '#type' => 'fieldset',
        '#title' => 'Precision settings for conversion from Integer to decimal.',
      ];
      $bundles = [];
      $field_name = $entity->getName();
      $entity_type = $entity->getTargetEntityTypeId();

      /** @var Drupal\Core\Entity\EntityTypeManager $entity_type_manager */
      $entity_type_manager = Drupal::service('entity_type.manager');
      $field_storage_definition = $entity_type_manager->getStorage('field_config')->load('node.page.field_age');

      if ($field_storage_definition) {
        $entity_field_definitions = Drupal::service('entity_field.manager');
        $bundle_info = Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
        foreach ($bundle_info as $bundle_name => $bundle) {
          $field_definitions = $entity_field_definitions->getFieldDefinitions($entity_type, $bundle_name);
          if (isset($field_definitions[$field_name])) {
            $bundles[] = $bundle_name;
          }
        }
      }
      $form['field_updater_settings']['entity_type'] = [
        '#type' => 'hidden',
        '#value' => $entity_type,
      ];

      $form['field_updater_settings']['bundles'] = [
        '#type' => 'hidden',
        '#value' => serialize($bundles),
      ];

      $form['field_updater_settings']['convert'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable integer to decimal conversion'),
      ];
      $form['field_updater_settings']['precision'] = [
        '#type' => 'select',
        '#title' => t('Precision'),
        '#options' => $precision,
        '#states' => [
          'visible' => [
            ':input[name="convert"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['field_updater_settings']['scale'] = [
        '#type' => 'select',
        '#title' => t('Scale'),
        '#options' => $scale,
        '#states' => [
          'visible' => [
            ':input[name="convert"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['actions']['submit']['#submit'][] = 'integer_to_decimal_field_storage_config_edit_form';
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function integer_to_decimal_field_storage_config_edit_form(array $form, FormStateInterface $form_state) {

  if ($form_state->getValue('convert')) {
    $form_object = $form_state->getFormObject();
    /** @var Drupal\field\Entity\FieldConfig $entity */
    $entity = $form_object->getEntity();
    $config = Drupal::configFactory();
    $data = $config->getEditable('field.storage.node.' . $entity->getName())
      ->getRawData();

    $entity_type = $data['entity_type'];
    $field_name = $data['field_name'];
    $precision = $form_state->getValue('precision');
    $scale = $form_state->getValue('scale');
    $bundles = unserialize($form_state->getValue('bundles'));

    /** @var Drupal\integer_to_decimal\Service\FieldUpdaterService $field_updater_service */
    $field_updater_service = Drupal::service('field_updater');
    try {
      $field_updater_service->fieldUpdater($field_name, 'decimal', $entity_type, $bundles, $precision, $scale);
    } catch (InvalidPluginDefinitionException|EntityStorageException|PluginNotFoundException $e) {
      Drupal::logger('integer_to_decimal')->notice($e->getMessage());
    } catch (Exception $e) {
    }
  }
}
